/********************
 * Global Variables *
 ********************/
var stingray = {
    dashboardID: window.localStorage.lastReportIdLoaded, //get the dashboard id instead from ... window.localStorage.lastReportIdLoaded
    colors: {
        DataPanel: getColor('.reportingToolboxDataSourceHeader', 'backgroundColor'),
        CompanyColor: getColor('.allegHeaderTab.mcxActiveTab', 'color'),
    },
    loadedCharts: [],
    loadedSources: [],
    charts: [],
    report: null,
    widgets: [],
    sources: [],
    startTime: 0,
    totalTime: 0
}




/*****************************
 * stingray Helper Functions *
 *****************************/

// Updates the Charts UI count
function updateChartCount() {
    $('#srcd_chart_count').html(stingray.loadedCharts.reduce(function(total, num) { return total + num; }) + " / " + stingray.loadedCharts.length);
    if (stingray.loadedCharts.reduce(function(total, num) { return total + num; }) === stingray.loadedCharts.length && stingray.loadedSources.reduce(function(total, num) { return total + num; }) === stingray.loadedSources.length) {
        stingray.totalTime = (performance.now() - stingray.startTime) / 1000;
    }
}

// Updates Data Sources UI count
function updateDataSourcesCount() {
    $('#srcd_data_sources').html(stingray.loadedSources.reduce(function(total, num) { return total + num; }) + " / " + stingray.loadedSources.length);
    if (stingray.loadedCharts.reduce(function(total, num) { return total + num; }) === stingray.loadedCharts.length && stingray.loadedSources.reduce(function(total, num) { return total + num; }) === stingray.loadedSources.length) {
        stingray.totalTime = (performance.now() - stingray.startTime) / 1000;
    }
}

function updateLongestLoad() {
    var theCharts = stingray.charts;
    var obj = {
        id: null,
        longestTime: 0,
        name: null
    }
    for (var i = 0; i < theCharts.length; i++) {
        var chartTime = (theCharts[i].Ts) ? timingGetData(theCharts[i].Ts) : 0;
        if (chartTime > obj.longestTime) {
            obj.longestTime = chartTime;
            obj.id = theCharts[i].ID;
            obj.name = theCharts[i].Name;
        }
    }
    $('#srcd_longest_load_id').html(obj.name);
    $('#srcd_longest_load_time').html(obj.longestTime);
}

// Toggles all stingRay UI components on/off
function stingRayToggle() {
    $('stingRay, stingRayChartbtn, stingRaydashboarddata, stingRaychartdata, stingRaySourcebtn').toggle();
    setChartDataPos();
}

// Returns the chart name from the chartID // chart names are only found from the getReports response
function findChartName(chartID) {
    var myCharts = stingray.report.Children[0].Children[0].Children;
    for (var i = 0; i < myCharts.length; i++) {
        if (chartID == myCharts[i].ID) {
            return myCharts[i].Name;
        }
    }
}

// Returns the Source name from the sourceID
function findSourceName(sourceID) {
    for (var x = 0; x < stingray.sources.length; x++) {
        if (stingray.sources[x][0]) {
            if (stingray.sources[x][0].ID === sourceID) {
                return stingray.sources[x][0].N
            }
        }
    }
}

// Called when user clicks on a chart button - Chart data section in the data panel is updated, when called
function updateChartData(id) {
    if (id == 'init') {
        //$('#srcd_name').html('Click on a chart to begin');
    }
    for (var i = 0; i < stingray.charts.length; i++) {
        var chart = stingray.charts[i];
        if (chart.ID == id) {
            $('#srcd_id_text').html(chart.ID);
            $('#srcd_name').html(chart.Name);
            $('#srcd_chartType').html(chart.D.chartType.toUpperCase());
            $('#srcd_metrics').html(chart.Ch.length);
            $('#srcd_breakdowns').html(checkBreakdownData(chart.BDs));
            $('#srcd_timing').html(timingGetData(chart.Ts));
            $('#srcd_hostname').html(chart.D.hostname);
            $('#srcd_vs').html(chart.Vs.length);
            break;
        }
    }
}

function clearBtnSelect() {
    $('stingraychartbtn').removeClass('selectedSRC');
}

// checks whether or not the chart has a breakdown and returns it's length or 0
function checkBreakdownData(BDs) {
    return (BDs ? BDs.length : 0);
}

function timingGetData(timings) {
    for (var i = 0; i < timings.length; i++) {
        if (timings[i].N == "ReportingController.ChartDataService.GetData") {
            return timings[i].S.toFixed(3);
        }
    }
}

// Encoding function for getChartData
function stingrayUrlencode(text) {
    return encodeURIComponent(text).replace(/!/g, '%21')
        .replace(/'/g, '%27')
        .replace(/\(/g, '%28')
        .replace(/\)/g, '%29')
        .replace(/\*/g, '%2A')
        .replace(/%20/g, '+');
}

// Decode function not currently being called
function stingrayUrldecode(text) {
    return decodeURIComponent((text + '').replace(/\+/g, '%20'));
}




/**********************
 * Get Data Functions *
 **********************/

// 1: First call initiated // Gets generic report data about the dashboard, including data source names and chart names and passes each to a function to get detailed data
function stingrayGetReportData() {
    stingray.startTime = performance.now();
    var getReport = new XMLHttpRequest();
    getReport.open("GET", "/Reporting/GetReport?reportId=" + stingray.dashboardID + "&profile=&_=0000000000000", true);
    getReport.onreadystatechange = function() {
        var sources;
        var zones;
        if (this.readyState == 4 && this.status == 200) {
            stingray.report = JSON.parse(JSON.parse(this.response).Value);
            stingray.report.Description = JSON.parse(stingray.report.Description);
            zones = JSON.parse(JSON.parse(this.response).Value).Children[0];
            sources = JSON.parse(JSON.parse(this.responseText).Value).Values;
            //Send an individual request
            for (var x = 0; x < sources.length; x++) {
                stingray.loadedSources[x] = 0;
                var element = sources[x];
                stingrayGetSourcesData(element, x);
            }
            stingrayStripCharts(zones);
        }
    }
    getReport.setRequestHeader("__RequestVerificationToken", $('[name="__RequestVerificationToken"]').val());
    getReport.send();
}

// Gets source data (aka: surveys/case programs) used in the dashboard
function stingrayGetSourcesData(element, sourcesIndex) {
    var getSources = new XMLHttpRequest();
    getSources.open("POST", "/Engage/GetSources", true);
    getSources.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            stingray.sources[sourcesIndex] = JSON.parse(this.responseText);
            stingray.loadedSources[sourcesIndex] = 1;
            updateDataSourcesCount();
        }
    };
    getSources.setRequestHeader("__RequestVerificationToken", $('[name="__RequestVerificationToken"]').val());
    getSources.setRequestHeader("Content-Type", "application/json");
    getSources.send('{"requests":[{"sourceId":' + element.SourceID + ',"dataType":' + (element.DataType ? element.DataType : "0") + '}]}');
}

// Finds chart nodes and then makes a call to get the chart data
function stingrayStripCharts(zone) {
    if (zone.ObjectType === 7) {
        //It's technically not a zone, it's a chart so we run the post request
        stingrayGetChartData(zone, stingray.charts.length);
        zone.Description = JSON.parse(zone.Description);
        if (zone.Children) {
            for (var x = 0; x < zone.Children.length; x++) {
                zone.Children[x].Description = JSON.parse(zone.Children[x].Description);
            }
        }
        stingray.loadedCharts[stingray.charts.length] = 0;
        stingray.charts.push(zone);
    } else if (zone.ObjectType === 10) {
        //It's a widget!!!
        zone.Description = JSON.parse(zone.Description);
        stingray.widgets.push(zone);
    } else {
        //It's a type of zone
        if (zone.Children) {
            for (var x = 0; x < zone.Children.length; x++) {
                stingrayStripCharts(zone.Children[x])
            }
        }
    }

}
// Gets chart data for every chart in the dashboard, regardless of whether they have been loaded
function stingrayGetChartData(chart, chartsIndex) {
    var getData = new XMLHttpRequest();
    getData.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) { //The request came through properly
            var chartInfo = JSON.parse(this.response);
            chartInfo.D = JSON.parse(chartInfo.D); //Parse the chart Description
            if (chartInfo.Ch) { //Parse the descriptions for all of the children
                for (var x = 0; x < chartInfo.Ch.length; x++) {
                    chartInfo.Ch[x].D = JSON.parse(chartInfo.Ch[x].D);
                }
            }
            stingray.charts[chartsIndex] = Object.assign({}, chartInfo, stingray.charts[chartsIndex]);
            stingray.loadedCharts[chartsIndex] = 1;
            //Calculate Chart complexity
            let numOfMetrics = 0;
            let numOfBreakDowns = 0;
            let chart_factor = 1;
            let question_factor = 1;
            if (stingray.charts[chartsIndex].Children) {
                for (let x = 0; x < stingray.charts[chartsIndex].Children.length; x++) {
                    if (stingray.charts[chartsIndex].Children[x].ObjectType === 14) {
                        question_factor = 3;
                    }
                    if (stingray.charts[chartsIndex].Children[x].Name === "Date" && stingray.charts[chartsIndex].Children[x].ID === -1 && question_factor < 3) {
                        question_factor = 2;
                    }
                    if (stingray.charts[chartsIndex].Children[x].Children) {
                        for (let y = 0; y < stingray.charts[chartsIndex].Children[x].Children.length; y++) {
                            if (stingray.charts[chartsIndex].Children[x].Children[y].ObjectType === 14) {
                                question_factor = 3;
                            }
                            if (stingray.charts[chartsIndex].Children[x].Children[y].Name === "Date" && stingray.charts[chartsIndex].Children[x].Children[y].ID === -1 && question_factor < 3) {
                                question_factor = 2;
                            }
                            numOfMetrics++;
                        }
                    }
                    if (stingray.charts[chartsIndex].Children[x].Breakdowns) {
                        for (let y = 0; y < stingray.charts[chartsIndex].Children[x].Breakdowns.length; y++) {
                            if (stingray.charts[chartsIndex].Children[x].Breakdowns[y].ObjectType === 14) {
                                question_factor = 3;
                            }
                            if (stingray.charts[chartsIndex].Children[x].Breakdowns[y].Name === "Date" && stingray.charts[chartsIndex].Children[x].Breakdowns[y].ID === -1 && question_factor < 3) {
                                question_factor = 2;
                            }
                            numOfMetrics++;
                        }
                    }
                    numOfMetrics++;
                }
            }
            if (stingray.charts[chartsIndex].Breakdowns) {
                for (let x = 0; x < stingray.charts[chartsIndex].Breakdowns.length; x++) {
                    if (stingray.charts[chartsIndex].Breakdowns[x].ObjectType === 14) {
                        question_factor = 3;
                    }
                    if (stingray.charts[chartsIndex].Breakdowns[x].Name === "Date" && stingray.charts[chartsIndex].Breakdowns[x].ID === -1 && question_factor < 3) {
                        question_factor = 2;
                    }
                    numOfBreakDowns++;
                }
            }
            stingray.charts[chartsIndex].complexityScore = (numOfMetrics + numOfBreakDowns) * chart_factor * question_factor;
            updateChartCount();
            updateLongestLoad();
        }
    };
    getData.open("POST", "/Reporting/GetData", true);
    getData.setRequestHeader("__RequestVerificationToken", $('[name="__RequestVerificationToken"]').val());
    getData.setRequestHeader("Accept", "application/json, text/javascript, */*; q=0.01");
    getData.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
    getData.setRequestHeader("Accept-Language", "en-US,en;q=0.9");
    //We need to do some adjustment on the actual request, so it matches what is actually requested when a chart loads
    if (chart.Children) {
        for (var x = 0; x < chart.Children.length; x++) {
            //Put the Dashboard GUID in each of the children's description
            var descriptionInfo;
            if (chart.Children[x].Description) {
                descriptionInfo = JSON.parse(chart.Children[x].Description);
            } else {
                descriptionInfo = {};
            }
            descriptionInfo.dashboardGUID = stingray.dashboardID;
            chart.Children[x].Description = JSON.stringify(descriptionInfo);
        }
    }
    //Verify the filters
    if (!chart.Filters) {
        chart.Filters = [
            { "ObjectType": 5, "Operator": 2 },
            { "ObjectType": 5, "Operator": 1 }
        ];
    }
    if (!chart.Flags) {
        chart.Flags = 8192;
    }
    if (!chart.SourceID) {
        chart.SourceID = 0;
    }
    getData.send("request=" + stingrayUrlencode(JSON.stringify(chart)) + "&profile=");
}




/***************************
 * Styling for UI Elements *
 ***************************/

var stingrayStyle = document.createElement('style');
var stingrayCSS = `
    #stingRayToggle {
        width:75px;
        height:33px;
        background:white;
        opacity:0.01;
    }

    stingRay {
        display:block;
        clear:both;
    }

    #stingRayMain {
        margin-bottom: 15px;
    }

    .stingRayContent {
        padding:5px;
        width:30%;
        height:250px;
        float:left;
        margin-left:0.5%;
    }

    .stingRayContent div div {
        font-size:24px;
        font-weight:bold;
    }

    stingRayChartbtn {
        position:relative;
        display:block;
        width: 33px;
        height: 33px;
        margin: 0 auto;
        font-size: 33px;
        z-index:1;
    }

    stingraychartdata, stingraydashboarddata {
        display: block;
        padding-bottom: 15px;
        margin-bottom: 2px;
    }

    stingRayChartbtn:hover {
        color: #494D4F !important;
    }

    #srcd_id {
        float:right;
        font-size: 11px;
        padding-right: 15px;
    }

    #srcd_name {
        font-weight: bold;
    }

    .leftStuff {
        width: 50%;
    }

    .rightStuff {
        text-align: left;
    }

    .selectedSRC {
        color: #F5921E !important;
    }
`;
stingrayStyle.appendChild(document.createTextNode(stingrayCSS));
document.getElementsByTagName('head')[0].appendChild(stingrayStyle);




/*************************************************
 * Create UI reporting element and append to DOM *
 *************************************************/

function createToggle() {
    $('#stingRayToggle').remove(); // Removes toggle button, if it exists.
    //Creates clickable square in data panel, to allow user to toggle stingRay on and off
    var parent = document.getElementsByClassName('reportingToolboxUList')[0];
    parent.setAttribute('id', 'dbelbtn');
    var newChild = document.createElement('li');
    newChild.setAttribute('id', 'stingRayToggle')
    newChild.setAttribute('onClick', 'stingRayToggle();');
    parent.appendChild(newChild);
}

function createChartBtn() {
    // Creates buttons for each chart and sets their chartid
    var parent = $('.reportingDataTileReport');
    for (var i = 0; i < parent.length; i++) {
        var newChild = document.createElement('stingRayChartbtn');
        newChild.setAttribute('chartID', parent[i].getAttribute('chartid'));
        newChild.setAttribute('data-header-icon', 'p');
        parent[i].childNodes[0].childNodes[1].appendChild(newChild);
    }
}

function createDashboardDataPanel() {
    $('stingraydashboarddata').remove(); // Removes this element, if it already exists in the DOM. Necessary if dashboard is changed.
    // Creates chart reporting area in data panel
    var parent = $('.reportingToolboxDataTree')[0];
    var newChild = document.createElement('stingRayDashboardData');
    newChild.setAttribute('id', 'stingRayDashboardData');
    newChild.innerHTML = `
        <h3 class="reportingToolboxDataSourceHeader" style="display: block;">
            <div class="reportingToolboxDataName fieldDescription">Dashboard Data</div>
        </h3>
        <div>
            <div>
                <table style="width:90%;margin: 0 auto;">
                    <tbody>
                    <tr>
                        <th></th>
                        <th></th>
                    </tr>
                    <tr>
                        <td class="leftStuff">Data Sources</td>
                        <td id="srcd_data_sources" class="rightStuff"></td>
                    </tr>
                    <tr>
                        <td class="leftStuff">Chart Count</td>
                        <td id="srcd_chart_count" class="rightStuff"></td>
                    </tr>
                    <tr>
                        <td class="leftStuff">Longest Load Chart</td>
                        <td id="srcd_longest_load_id" class="rightStuff"></td>
                    </tr>
                    <tr>
                        <td class="leftStuff">Longest Load Time</td>
                        <td id="srcd_longest_load_time" class="rightStuff"></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    `
    parent.appendChild(newChild);
}

function createChartDataPanel() {
    $('stingraychartdata').remove(); // Removes this element, if it already exists in the DOM. Necessary if dashboard is changed.
    // Creates chart reporting area in data panel
    var parent = $('.reportingToolboxDataTree')[0];
    var newChild = document.createElement('stingRaychartdata');
    newChild.setAttribute('id', 'stingRayChartData');
    newChild.innerHTML = `
        <h3 class="reportingToolboxDataSourceHeader" style="display: block;">
            <div class="reportingToolboxDataName fieldDescription">Chart Data</div>
            <span id="srcd_id">ID: <span id="srcd_id_text"></span></span>
        </h3>
        <div id="stingRayChartDataDiv">
            <div>
                <table style="width:90%;margin: 0 auto;">
                    <tbody>
                    <tr>
                        <th></th>
                        <th></th>
                    </tr>
                    <tr>
                        <td class="leftStuff">Chart Name</td>
                        <td id="srcd_name" class="rightStuff"></td>
                    </tr>
                    <tr>
                        <td class="leftStuff">Chart Type</td>
                        <td id="srcd_chartType" class="rightStuff"></td>
                    </tr>
                    <tr>
                        <td class="leftStuff">Metrics</td>
                        <td id="srcd_metrics" class="rightStuff"></td>
                    </tr>
                    <tr>
                        <td class="leftStuff">Breakdowns</td>
                        <td id="srcd_breakdowns" class="rightStuff"></td>
                    </tr>
                    <tr>
                        <td class="leftStuff">Timing</td>
                        <td id="srcd_timing" class="rightStuff"></td>
                    </tr>
                    <tr>
                        <td class="leftStuff">Hostname</td>
                        <td id="srcd_hostname" class="rightStuff"></td>
                    </tr>
                    <tr>
                        <td class="leftStuff">Vs Count</td>
                        <td id="srcd_vs" class="rightStuff"></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    `
    parent.appendChild(newChild);
}

function createFunctionDataPanel() {
    $('stingrayfunctiondata').remove(); // Removes this element, if it already exists in the DOM. Necessary if dashboard is changed.
    // Creates chart reporting area in data panel
    var parent = $('.reportingToolboxDataTree')[0];
    var newChild = document.createElement('stingRaychartdata');
    newChild.setAttribute('id', 'stingRayChartData');
    newChild.innerHTML = `
        <h3 class="reportingToolboxDataSourceHeader" style="display: block;">
            <div class="reportingToolboxDataName fieldDescription">Functions</div>
        </h3>
        <div>
            <button id="srReportBtn" type="button" onclick="generateReport()">Generate Report</button>
            <button id="srTreeBtn" type="button" onclick="createDataTree()">Go to Data Tree</button>
        </div>
    `
    parent.appendChild(newChild);
}

function getColor(selector, attribute) {
    var elem = document.querySelector(selector);
    var style = getComputedStyle(elem);
    return style[attribute];
}

// Ensures chart data position is the last child, so that it appears at the bottom of the sources in the data panel
function setChartDataPos() {
    $('.reportingToolboxDataTree').append($('#stingRayDashboardData'));
    $('.reportingToolboxDataTree').append($('#stingRayChartData'));
}




/****************************
 * HTML for Detailed Report *
 ****************************/

function generateReport() {
    var generatedReportString;
    generatedReportString = startHTMLReport();
    setTimeout(function() { generatedReportString += getGeneralDashboardReport(); }, 10);
    setTimeout(function() { generatedReportString += getDataSourcesReport(); }, 15);
    setTimeout(function() { generatedReportString += getChartsReport(); }, 20);
    setTimeout(function() { generatedReportString += getWidgetsReport(); }, 25);
    setTimeout(function() { generatedReportString += endHTMLReport(); }, 30);
    setTimeout(function() {
        newwindow = window.open();
        newdocument = newwindow.document;
        newdocument.write(generatedReportString);
        newdocument.close();
    }, 50);
}

function startHTMLReport() {
    return `
<!DOCTYPE html>
<html>
<head>
<meta content="text/html; charset=UTF-8" http-equiv="content-type">
<style>
    :root {
        --primary: #8AC452;
        --secondary1: #494D4F;
        --secondary2: #9EA4A2;
        --secondary3: #E6E6E6;
        --tertiary: #F5921E;
    }
    body {
        color: var(--secondary1);
        font-family:Arial;
        background: var(--secondary3);
        box-sizing: border-box;
    }
    stingrayreport {
        display: block;
        width: 1300px;
        margin: 0 auto;
        padding: 0px 20px;
    }
    h1 {
        text-align: center;
        background: #FFF;
        border-radius: 10px;
        margin: 0px;
        padding: 10px 20px;
    }
    h2 {
        color: var(--primary);
        text-align: center;
        margin-top: 0px;
    }
    stingraysection {
        display: block;
        background: #FFF;
        border-radius: 10px;
        padding: 15px;
        margin: 7px 0px;
        overflow: hidden;
    }
    stingraySourcePanel {
        width: 600px;
        height: 500px;
        float: left;
    }
    stingrayChartPanel {
        width: 600px;
        margin: 15px;
        float: left;
    }
</style>
<title>Dashboard Report</title>
</head>
<body>
<stingRayReport>
`;
}

function getGeneralDashboardReport() {
    //Dashboard Level Filters
    var filtersCount = 0;
    if (stingray.report.Filters) {
        if (stingray.report.Filters[0]) {
            if (stingray.report.Filters[0].Filters) {
                filtersCount += stingray.report.Filters[0].Filters.length;
            }
        }
        if (stingray.report.Filters[1]) {
            if (stingray.report.Filters[1].Filters) {
                filtersCount += stingray.report.Filters[1].Filters.length;
            }
        }
    }
    //Dashboard Level Date Filters
    if (stingray.report.Description.datePreset) {
        var dateFilter = getDateFilterInterpretation(stingray.report.Description.dateType, JSON.parse(stingray.report.Description.datePreset));
    }
    //Dashboard Estimated chart load time
    var loadTime = 0;
    for (var x = 0; x < stingray.charts.length; x++) {
        loadTime += (stingray.charts[x].Ts) ? stingray.charts[x].Ts[stingray.charts[x].Ts.length - 1].S : 0;
    }
    //Dashboard level complexity calculation
    var complexityCalculation = 0;
    for (var x = 0; x < stingray.charts.length; x++) {
        complexityCalculation += (stingray.charts[x].complexityScore);
    }
    //Do some fancy calculation stuffage here.
    var HTMLstring = "";
    HTMLstring = HTMLstring + "<h1>" + stingray.report.Name + "</h1>" +
        "<stingRaySection>" +
        "<h2>Dashboard Complexity</h2>" +
        "<p>Site: " + AllegianceCore.Header.headerData.surveyHubUrl.split("/")[2] + "</p>" +
        "<p>Access User: " + $("#profileIcon").html() + "</p>" +
        "<img src='" + window.customerImageUrl + "' height='100' style='float:right'>" +
        "</br>" +
        "<p>GUID: " + stingray.dashboardID + "</p>" +
        "<p>Data Sources Count: " + stingray.sources.length + "</p>" +
        "<p>Charts Count: " + stingray.charts.length + "</p>" +
        "<p>Filters Count: " + filtersCount + "</p>" +
        "<p>Widgets Count: " + stingray.widgets.length + "</p>" +
        "<p>Server Strain: " + loadTime + " Seconds</p>" +
        "<p>Estimated Load Time: " + stingray.totalTime + "</p>" +
        "<p>Calculated Complexity: " + complexityCalculation + "</p>" +
        "<p>Owner: " + stingray.report.Description.ownerName + "</p>" +
        "<p>ID: " + stingray.report.ID + "</p>" +
        "<p>Date Filter: " + dateFilter + "</p>" +
        "<p>Date Filter: " + dateFilter + "</p>" +
        "<p>Window Language: " + window.lang + " // " + window.langId + "</p>" +
        "</stingRaySection>";
    return HTMLstring;
}

function getDateFilterInterpretation(dateType, datePreset) {
    var dateFilter = "";
    switch (dateType) {
        case 5:
            switch (datePreset) {
                case 11:
                    dateFilter = "Preset: All Dates";
                    break;
                default:
                    dateFilter = "Preset: Unknown " + datePreset;
                    break;
            }
            break;
        default:
            dateFilter = "Unknown Date Filter " + dateType;
            break;
    }
    return dateFilter;
}

function getFilters(filters) {
    var result = "";
    if (filters) {
        if (filters[0] && filters[0].Filters) {
            //Iterate through the "any" filters
            for (var z = 0; z < filters[0].Filters.length; z++) {}
        }
        if (filters[1] && filters[1].Filters) {
            //Iterate through the "any" filters
            for (var z = 0; z < filters[1].Filters.length; z++) {}
        }
    }
    return result;
}

function getDataSourcesReport() {
    var surveyCount = 0;
    var caseCount = 0;
    var removedCount = 0;
    for (var x = 0; x < stingray.sources.length; x++) {
        if (stingray.sources[x][0]) {
            if (stingray.sources[x][0].D === '{"AnswerDataType":"null"}') {
                //Survey
                surveyCount += 1;
                sourceType = "Survey";
            } else {
                //Case
                caseCount += 1;
                sourceType = "Case Program";
            }
        } else {
            removedCount += 1;
        }
    }
    //Start to Compile our HTML
    var HTMLstring = "";
    HTMLstring = HTMLstring +
        "<stingRaySection>" +
        "<h2>Data Sources</h2>" +
        "<p>Total Surveys: " + surveyCount + "</p>" +
        "<p>Total Case Programs: " + caseCount + "</p>" +
        "<p>Total Removed Data Sources: " + removedCount + "</p>";
    //Loop through all data sources, appending to the HTML as we go
    for (var x = 0; x < stingray.sources.length; x++) {
        //initialize our vars
        var answerOptionCount = 0;
        var parentName = "";
        var parentID = "";
        var tokenTypeCount = [0, 0, 0, 0, 0, 0, 0, 0, 0]; //See getSourceTokenType Function for description on what they represent
        var childrenCount = 0;
        var sourceType = "";
        //Find the differences between sources
        if (stingray.sources[x][0]) {
            if (stingray.sources[x][0].D === '{"AnswerDataType":"null"}') {
                //Survey
                sourceType = "Survey";
            } else {
                //Case
                sourceType = "Case Program";
            }
            //Find out the parent info
            if (stingray.sources[x][0].PID) {
                parentName = findSourceName(stingray.sources[x][0].PID);
                parentID = stingray.sources[x][0].PID;
            }
            //Loop through each of the children in the data source
            if (stingray.sources[x][0].Ch) {
                for (var z = 0; z < stingray.sources[x][0].Ch.length; z++) {
                    tokenTypeCount[stingray.sources[x][0].Ch[z].ST]++; //Increment the count of the type of token it is
                    if (stingray.sources[x][0].Ch[z].C) {
                        answerOptionCount += stingray.sources[x][0].Ch[z].C; //Add it's children to the answer option count (These are answer options)
                    }
                    //If it's a table object, get information about those objects as well
                    if (stingray.sources[x][0].Ch[z].ST === 8) {
                        tokenTypeCount[0] += stingray.sources[x][0].Ch[z].Ch.length;
                        stingray.sources[x][0].Ch[z].Ch.forEach(function(element) {
                            if (element.C) {
                                answerOptionCount += element.C; //Add to the answer option count
                            }
                        });
                    }
                }
            }
            //Find out how many sources this particular source has as children
            for (var y = 0; y < stingray.sources.length; y++) {
                if (stingray.sources[y].PID === stingray.sources[x][0].ID) {
                    childrenCount++;
                }
            }
            //Append that data to the HTML
            HTMLstring = HTMLstring + "<stingrayChartPanel>" +
                "<h3>" + stingray.sources[x][0].N + "</h3>" +
                "<p>ID: " + stingray.sources[x][0].ID + "</p>" +
                "<p>Type: " + sourceType + "</p>";
        }
        if (parentName + " / " + parentID != " / ") {
            HTMLstring = HTMLstring + "<p>Parent Info: " + parentName + " / " + parentID + "</p>";
        }
        if (childrenCount != 0) {
            HTMLstring = HTMLstring + "<p>Children: " + childrenCount + "</p>";
        }
        //list out each token if it's value is more than 0
        for (var y = 0; y < tokenTypeCount.length; y++) {
            if (tokenTypeCount[y]) {
                HTMLstring = HTMLstring + "<p>" + getSourceTokenType(y, sourceType) + ": " + tokenTypeCount[y] + "</p>";
            }
        }
        HTMLstring = HTMLstring +
            "<p>Total Tokens: " + tokenTypeCount.reduce(function(total, num) { return total + num; }) + "</p>" +
            "<p>Answer Options: " + answerOptionCount + "</p>" +
            "</stingrayChartPanel>";
    }
    //Close up our HTML
    HTMLstring = HTMLstring + "</stingRaySection>";
    return HTMLstring;
}

function getSourceTokenType(tokenNumber, sourceType) {
    if (sourceType === "Survey") {
        switch (tokenNumber) {
            case 0:
                return "Sub-Table Tokens";
            case 1:
                return "Long Text Tokens";
            case 2:
                return "Choose Many Tokens";
            case 3:
                return "Rank Tokens";
            case 4:
                return "Choose One / Single Checkbox Tokens";
            case 5:
                return "Short Text / File Upload / Numeric / Date Tokens";
            case 6:
                return "Hierarchy Tokens";
            case 7:
                return "7 Tokens";
            case 8:
                return "Table Tokens";
        }
    } else {
        switch (tokenNumber) {
            case 0:
                return "Sub-Table Tokens";
            case 1:
                return "Long Text Tokens";
            case 2:
                return "Choose Many Tokens";
            case 3:
                return "Rank Tokens";
            case 4:
                return "Choose One / Status Tokens";
            case 5:
                return "Short Text / Numeric / Date Tokens";
            case 6:
                return "Hierarchy Tokens";
            case 7:
                return "7 Tokens";
            case 8:
                return "Table Tokens";
        }
    }
}

function getChartsReport() {
    var HTMLstring = "";
    //----Chart Header----\\
    HTMLstring = HTMLstring +
        "<stingraysection>" +
        "<h2>Charts Overview</h2>";
    var chartTypesCount = window.chartTypes;
    for (var x in chartTypesCount) {
        chartTypesCount[x].quantityInDB = 0;
    }
    chartTypesCount["error"] = {};
    chartTypesCount["error"].quantityInDB = 0;
    chartTypesCount["other"] = {};
    chartTypesCount["other"].quantityInDB = 0;
    var chartMetricCount = 0;
    var chartSplitCount = 0;
    var chartSplitMetricCount = 0;
    var chartBreakdownCount = 0;
    var chartFilterCount = 0;
    for (var x = 0; x < stingray.charts.length; x++) {
        if (stingray.charts[x].E) {
            chartTypesCount["error"].quantityInDB++;
        } else if (chartTypesCount[stingray.charts[x].Description.chartType]) {
            chartTypesCount[stingray.charts[x].Description.chartType].quantityInDB++;
        } else {
            chartTypesCount["other"].quantityInDB++;
        }
        if (stingray.charts[x].Children) {
            chartMetricCount += stingray.charts[x].Children.length;
        }
        if (stingray.charts[x].Ch) {
            chartSplitMetricCount += stingray.charts[x].Ch.length;
            if (stingray.charts[x].BDs) {
                chartBreakdownCount += stingray.charts[x].BDs.length;
            }
        }
        if (stingray.charts[x].Filters[1]) {
            if (stingray.charts[x].Filters[1].Filters) {
                chartFilterCount += stingray.charts[x].Filters[1].Filters.length;
            }
        }
        if (stingray.charts[x].Filters[0]) {
            if (stingray.charts[x].Filters[0].Filters) {
                chartFilterCount += stingray.charts[x].Filters[0].Filters.length;
            }
        }
    }
    HTMLstring = HTMLstring +
        "<stingrayChartPanel>" +
        "<p>Bar Charts: " + chartTypesCount["bar"].quantityInDB + "</p>" +
        "<p>Column Charts: " + chartTypesCount["column"].quantityInDB + "</p>" +
        "<p>Line Charts: " + chartTypesCount["line"].quantityInDB + "</p>" +
        "<p>Big Number Charts: " + chartTypesCount["number"].quantityInDB + "</p>" +
        "<p>Pie Charts: " + chartTypesCount["pie"].quantityInDB + "</p>" +
        "<p>Stacked Bar Charts: " + chartTypesCount["sbar"].quantityInDB + "</p>" +
        "<p>Stacked Column Charts: " + chartTypesCount["scolumn"].quantityInDB + "</p>" +
        "<p>Table Charts: " + chartTypesCount["table"].quantityInDB + "</p>";
    if (chartTypesCount["error"].quantityInDB) {
        HTMLstring = HTMLstring + "<p>Error Charts: " + chartTypesCount["error"].quantityInDB + "</p>";
    }
    if (chartTypesCount["other"].quantityInDB) {
        HTMLstring = HTMLstring + "<p>Other Charts: " + chartTypesCount["other"].quantityInDB + "</p>";
    }
    HTMLstring = HTMLstring +
        "</stingrayChartPanel>";
    for (var x = 0; x < stingray.charts.length; x++) {}
    HTMLstring = HTMLstring +
        "<stingrayChartPanel>" +
        "<p>Pre-Split Metrics: " + chartMetricCount + "</p>" +
        "<p>Splits: " + chartSplitCount + "</p>" +
        "<p>Post-Split Metrics: " + chartSplitMetricCount + "</p>" +
        "<p>Breakdowns: " + chartBreakdownCount + "</p>" +
        "<p>Dashboard Filters: " + chartFilterCount + "</p>" +
        "</stingrayChartPanel>" +
        "</stingraysection>";
    //----Chart Details----\\
    for (var x = 0; x < stingray.charts.length; x++) {
        HTMLstring = HTMLstring +
            "<stingraysection>";
        HTMLstring += (stingray.charts[x].Name ? ("<h2>Chart: " + stingray.charts[x].Name + "</h2>") : "");
        HTMLstring += (stingray.charts[x].Description.chartType ? ("<p>Chart Type: " + stingray.charts[x].Description.chartType + "</p>") : "");
        //Number of Splits
        HTMLstring += (stingray.charts[x].Ch ? ("<p>Post-Mach Metrics: " + stingray.charts[x].Ch.length + "</p>") : "");
        HTMLstring += (stingray.charts[x].BDs ? ("<p>Breakdowns: " + stingray.charts[x].BDs.length + "</p>") : "");
        HTMLstring += (stingray.charts[x].Filters.length ? ("<p>Chart Filters.length: " + stingray.charts[x].Filters.length + "</p>") : "");
        HTMLstring += (stingray.charts[x].D.cached ? ("<p>Chart D.cached: " + stingray.charts[x].D.cached + "</p>") : "");
        HTMLstring += (stingray.charts[x].D.hostname ? ("<p>Chart D.hostname: " + stingray.charts[x].D.hostname + "</p>") : "");
        HTMLstring += (stingray.charts[x].GUID ? ("<p>Chart GUID: " + stingray.charts[x].GUID + "</p>") : "");
        HTMLstring += (stingray.charts[x].ID ? ("<p>Chart ID: " + stingray.charts[x].ID + "</p>") : "");
        HTMLstring += (stingray.charts[x].Ts ? ("<p>Timing: " + stingray.charts[x].Ts[stingray.charts[x].Ts.length - 1].S + "</p>") : "");
        HTMLstring += (stingray.charts[x].Ts ? ("<p>Score: " + stingray.charts[x].complexityScore + "</p>") : "");

        /* //This is the extensive chart mockup.  Nearly all of this is covered in the tree graph.
        HTMLstring = HTMLstring + (stingray.charts[x].BD ? ("<p>Chart BD: " + stingray.charts[x].BD + "</p>") : "");
        HTMLstring = HTMLstring + (stingray.charts[x].BDs ? ("<p>Chart BDs: " + stingray.charts[x].BDs + "</p>") : "");
        HTMLstring = HTMLstring + (stingray.charts[x].BDs ? ("<p>Chart Breakdowns: " + stingray.charts[x].BDs.length + "</p>") : "");
        HTMLstring = HTMLstring + (stingray.charts[x].Children ? ("<p>Chart Children: " + stingray.charts[x].Children + "</p>") : "");
        HTMLstring = HTMLstring + (stingray.charts[x].Ch ? ("<p>Chart Ch: " + stingray.charts[x].Ch + "</p>") : "");
        HTMLstring = HTMLstring + (stingray.charts[x].D.DCLenabled ? ("<p>Chart D.DCLenabled: " + stingray.charts[x].D.DCLenabled + "</p>") : "");
        HTMLstring = HTMLstring + (stingray.charts[x].D.DateTimeFormat ? ("<p>Chart D.DateTimeFormat: " + stingray.charts[x].D.DateTimeFormat + "</p>") : "");
        HTMLstring = HTMLstring + (stingray.charts[x].D.cacheKey ? ("<p>Chart D.cacheKey: " + stingray.charts[x].D.cacheKey + "</p>") : "");
        HTMLstring = HTMLstring + (stingray.charts[x].D.cached ? ("<p>Chart D.cached: " + stingray.charts[x].D.cached + "</p>") : "");
        HTMLstring = HTMLstring + (stingray.charts[x].D.staleCache ? ("<p>Chart D.staleCache: " + stingray.charts[x].D.staleCache + "</p>") : "");
        HTMLstring = HTMLstring + (stingray.charts[x].D.isDefaultTitle ? ("<p>Chart D.isDefaultTitle: " + stingray.charts[x].D.isDefaultTitle + "</p>") : "");
        HTMLstring = HTMLstring + (stingray.charts[x].D.hostname ? ("<p>Chart D.hostname: " + stingray.charts[x].D.hostname + "</p>") : "");
        HTMLstring = HTMLstring + (stingray.charts[x].D.pageSize ? ("<p>Chart D.pageSize: " + stingray.charts[x].D.pageSize + "</p>") : "");
        if (stingray.charts[x].Description.yAxis) {
            if (stingray.charts[x].Description.yAxis.title) {
                HTMLstring = HTMLstring + (stingray.charts[x].Description.yAxis.title.text ? ("<p>Chart Description yAxis title.text:" + stingray.charts[x].Description.yAxis.title.text + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Description.yAxis.title.text ? ("<p>Chart Description yAxis title.text:" + stingray.charts[x].Description.yAxis.title.text + "</p>") : "");
            }
            HTMLstring = HTMLstring + (stingray.charts[x].Description.yAxis.plotLines ? ("<p>Chart Description yAxis plotLines:" + stingray.charts[x].Description.yAxis.plotLines + "</p>") : "");
            HTMLstring = HTMLstring + (stingray.charts[x].Description.yAxis.offset ? ("<p>Chart Description yAxis offset:" + stingray.charts[x].Description.yAxis.offset + "</p>") : "");
            HTMLstring = HTMLstring + (stingray.charts[x].Description.yAxis.index ? ("<p>Chart Description yAxis index:" + stingray.charts[x].Description.yAxis.index + "</p>") : "");
            if (stingray.charts[x].Description.yAxis.labels) {
                HTMLstring = HTMLstring + (stingray.charts[x].Description.yAxis.labels.enabled ? ("<p>Chart Description yAxis labels.enabled:" + stingray.charts[x].Description.yAxis.labels.enabled + "</p>") : "");
                if (stingray.charts[x].Description.yAxis.style) {
                    HTMLstring = HTMLstring + (stingray.charts[x].Description.yAxis.labels.style.fontSize ? ("<p>Chart Description yAxis labels.style.fontSize:" + stingray.charts[x].Description.yAxis.labels.style.fontSize + "</p>") : "");
                }
            }
            HTMLstring = HTMLstring + (stingray.charts[x].Description.yAxis.goalValues ? ("<p>Chart Description yAxis goalValues:" + stingray.charts[x].Description.yAxis.goalValues + "</p>") : "");
            HTMLstring = HTMLstring + (stingray.charts[x].Description.yAxis.lineWidth ? ("<p>Chart Description yAxis lineWidth:" + stingray.charts[x].Description.yAxis.lineWidth + "</p>") : "");
            HTMLstring = HTMLstring + (stingray.charts[x].Description.yAxis.minorGridLineWidth ? ("<p>Chart Description yAxis minorGridLineWidth:" + stingray.charts[x].Description.yAxis.minorGridLineWidth + "</p>") : "");
            HTMLstring = HTMLstring + (stingray.charts[x].Description.yAxis.gridLineWidth ? ("<p>Chart Description yAxis gridLineWidth:" + stingray.charts[x].Description.yAxis.gridLineWidth + "</p>") : "");
            HTMLstring = HTMLstring + (stingray.charts[x].Description.yAxis.tickInterval ? ("<p>Chart Description yAxis tickInterval:" + stingray.charts[x].Description.yAxis.tickInterval + "</p>") : "");
            HTMLstring = HTMLstring + (stingray.charts[x].Description.yAxis.min ? ("<p>Chart Description yAxis min:" + stingray.charts[x].Description.yAxis.min + "</p>") : "");
            HTMLstring = HTMLstring + (stingray.charts[x].Description.yAxis.max ? ("<p>Chart Description yAxis max:" + stingray.charts[x].Description.yAxis.max + "</p>") : "");
        }
        HTMLstring = HTMLstring + (stingray.charts[x].ED ? ("<p>Chart ED: " + stingray.charts[x].ED + "</p>") : "");
        HTMLstring = HTMLstring + (stingray.charts[x].E ? ("<p>Chart E: " + stingray.charts[x].E + "</p>") : "");
        HTMLstring = HTMLstring + (stingray.charts[x].Fl ? ("<p>Chart Fl: " + stingray.charts[x].Fl + "</p>") : "");
        HTMLstring = HTMLstring + (stingray.charts[x].Filters.length ? ("<p>Chart Filters.length: " + stingray.charts[x].Filters.length + "</p>") : "");
        HTMLstring = HTMLstring + (stingray.charts[x].LocalizationKey ? ("<p>Chart LocalizationKey: " + stingray.charts[x].LocalizationKey + "</p>") : "");
        HTMLstring = HTMLstring + (stingray.charts[x].GUID ? ("<p>Chart GUID: " + stingray.charts[x].GUID + "</p>") : "");
        HTMLstring = HTMLstring + (stingray.charts[x].ID ? ("<p>Chart ID: " + stingray.charts[x].ID + "</p>") : "");
        if (stingray.charts[x].Ts) {
            for (var y = 0; y < stingray.charts[x].Ts.length; y++) {
                HTMLstring = HTMLstring + (stingray.charts[x].Ts[y].N ? ("<p>Post-Mach Metric Ch Ts N: " + stingray.charts[x].Ts[y].N + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Ts[y].S ? ("<p>Post-Mach Metric Ch Ts S: " + stingray.charts[x].Ts[y].S + "</p>") : "");
            }
        }
        HTMLstring = HTMLstring + (stingray.charts[x].VID ? ("<p>Chart VID: " + stingray.charts[x].VID + "</p>") : "");
        if (stingray.charts[x].Vs) {
            for (var y = 0; y < stingray.charts[x].Vs.length; y++) {
                HTMLstring = HTMLstring + (stingray.charts[x].Vs[y].ID ? ("<p>Post-Mach Metric Ch Vs ID: " + stingray.charts[x].Vs[y].ID + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Vs[y].GUID ? ("<p>Post-Mach Metric Ch Vs GUID: " + stingray.charts[x].Vs[y].GUID + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Vs[y].SID ? ("<p>Post-Mach Metric Ch Vs SID: " + stingray.charts[x].Vs[y].SID + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Vs[y].OT ? ("<p>Post-Mach Metric Ch Vs OT: " + stingray.charts[x].Vs[y].OT + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Vs[y].N ? ("<p>Post-Mach Metric Ch Vs N: " + stingray.charts[x].Vs[y].N + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Vs[y].D ? ("<p>Post-Mach Metric Ch Vs D: " + stingray.charts[x].Vs[y].D + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Vs[y].V ? ("<p>Post-Mach Metric Ch Vs V: " + stingray.charts[x].Vs[y].V + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Vs[y].ED ? ("<p>Post-Mach Metric Ch Vs ED: " + stingray.charts[x].Vs[y].ED + "</p>") : "");
            }
        }
        HTMLstring = HTMLstring + ("<p>Chart Date Filter: " + getDateFilterInterpretation(stingray.charts[x].D.dateType, stingray.charts[x].D.datePreset, stingray.charts[x].D.dateRelativeNum, stingray.charts[x].D.dateRelativeType) + "</p>");
        //Getting all of the data for the Metric Level Info
        if (stingray.charts[x].Children) {
            for (var y = 0; y < stingray.charts[x].Children.length; y++) {
                HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Action ? ("<p>Pre-Mach Metric Action: " + stingray.charts[x].Children[y].Action + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Count ? ("<p>Pre-Mach Metric Count: " + stingray.charts[x].Children[y].Count + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Description.AnswerDataType ? ("<p>Pre-Mach Metric Description.AnswerDataType: " + stingray.charts[x].Children[y].Description.AnswerDataType + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Description.showGoal ? ("<p>Pre-Mach Metric Description.showGoal: " + stingray.charts[x].Children[y].Description.showGoal + "</p>") : "");
                HTMLstring = HTMLstring + ("<p>Pre-Mach Metric Date Filter: " + getDateFilterInterpretation(stingray.charts[x].Children[y].Description.dateType, stingray.charts[x].Children[y].Description.datePreset, stingray.charts[x].Children[y].Description.dateRelativeNum, stingray.charts[x].Children[y].Description.dateRelativeNum) + "</p>");
                HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Description.dashboardGUID ? ("<p>Pre-Mach Metric Description.dashboardGUID: " + stingray.charts[x].Children[y].Description.dashboardGUID + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Description.savedFilterId ? ("<p>Pre-Mach Metric Description.savedFilterId: " + stingray.charts[x].Children[y].Description.savedFilterId + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Description.aggFunc ? ("<p>Pre-Mach Metric Description.aggFunc: " + stingray.charts[x].Children[y].Description.aggFunc + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Description.aggCalc ? ("<p>Pre-Mach Metric Description.aggCalc: " + stingray.charts[x].Children[y].Description.aggCalc + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Description.attributeNameOverride ? ("<p>Pre-Mach Metric Description.attributeNameOverride: " + stingray.charts[x].Children[y].Description.attributeNameOverride + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Flags ? ("<p>Pre-Mach Metric Flags: " + stingray.charts[x].Children[y].Flags + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Children[y].GUID ? ("<p>Pre-Mach Metric GUID: " + stingray.charts[x].Children[y].GUID + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Children[y].ID ? ("<p>Pre-Mach Metric ID: " + stingray.charts[x].Children[y].ID + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Children[y].LocalizationKey ? ("<p>Pre-Mach Metric LocalizationKey: " + stingray.charts[x].Children[y].LocalizationKey + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Name ? ("<p>Pre-Mach Metric Name: " + stingray.charts[x].Children[y].Name + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Children[y].ObjectType ? ("<p>Pre-Mach Metric ObjectType: " + stingray.charts[x].Children[y].ObjectType + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Operator ? ("<p>Pre-Mach Metric Operator: " + stingray.charts[x].Children[y].Operator + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Children[y].ParentID ? ("<p>Pre-Mach Metric ParentID: " + stingray.charts[x].Children[y].ParentID + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Children[y].ScaleType ? ("<p>Pre-Mach Metric ScaleType: " + stingray.charts[x].Children[y].ScaleType + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Children[y].SourceID ? ("<p>Pre-Mach Metric SourceID: " + stingray.charts[x].Children[y].SourceID + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Value ? ("<p>Pre-Mach Metric Value: " + stingray.charts[x].Children[y].Value + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Children[y].ValueID ? ("<p>Pre-Mach Metric ValueID: " + stingray.charts[x].Children[y].ValueID + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Children[y].ValueType ? ("<p>Pre-Mach Metric ValueType: " + stingray.charts[x].Children[y].ValueType + "</p>") : "");
                if (stingray.charts[x].Children[y].Breakdowns) {
                    for (var z = 0; z < stingray.charts[x].Children[y].Breakdowns.length; z++) {
                        HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Breakdowns[z].ID ? ("<p>Pre-Mach Metric Breakdown ID: " + stingray.charts[x].Children[y].Breakdowns[z].ID + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Breakdowns[z].Action ? ("<p>Pre-Mach Metric Breakdown Action: " + stingray.charts[x].Children[y].Breakdowns[z].Action + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Breakdowns[z].Count ? ("<p>Pre-Mach Metric Breakdown Count: " + stingray.charts[x].Children[y].Breakdowns[z].Count + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Breakdowns[z].Description ? ("<p>Pre-Mach Metric Breakdown Description: " + stingray.charts[x].Children[y].Breakdowns[z].Description + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Breakdowns[z].Flags ? ("<p>Pre-Mach Metric Breakdown Flags: " + stingray.charts[x].Children[y].Breakdowns[z].Flags + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Breakdowns[z].Flags2 ? ("<p>Pre-Mach Metric Breakdown Flags2: " + stingray.charts[x].Children[y].Breakdowns[z].Flags2 + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Breakdowns[z].GUID ? ("<p>Pre-Mach Metric Breakdown GUID: " + stingray.charts[x].Children[y].Breakdowns[z].GUID + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Breakdowns[z].Name ? ("<p>Pre-Mach Metric Breakdown Name: " + stingray.charts[x].Children[y].Breakdowns[z].Name + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Breakdowns[z].ObjectType ? ("<p>Pre-Mach Metric Breakdown ObjectType: " + stingray.charts[x].Children[y].Breakdowns[z].ObjectType + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Breakdowns[z].ParentID ? ("<p>Pre-Mach Metric Breakdown ParentID: " + stingray.charts[x].Children[y].Breakdowns[z].ParentID + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Breakdowns[z].ScaleType ? ("<p>Pre-Mach Metric Breakdown ScaleType: " + stingray.charts[x].Children[y].Breakdowns[z].ScaleType + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Breakdowns[z].SourceID ? ("<p>Pre-Mach Metric Breakdown SourceID: " + stingray.charts[x].Children[y].Breakdowns[z].SourceID + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Breakdowns[z].Value ? ("<p>Pre-Mach Metric Breakdown Value: " + stingray.charts[x].Children[y].Breakdowns[z].Value + "</p>") : "");
                    }
                }
                if (stingray.charts[x].Children[y].Children) {
                    for (var z = 0; z < stingray.charts[x].Children[y].Children.length; z++) {
                        HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Children[z].ID ? ("<p>Pre-Mach Metric Children ID: " + stingray.charts[x].Children[y].Children[z].ID + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Children[z].Action ? ("<p>Pre-Mach Metric Children Action: " + stingray.charts[x].Children[y].Children[z].Action + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Children[z].Count ? ("<p>Pre-Mach Metric Children Count: " + stingray.charts[x].Children[y].Children[z].Count + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Children[z].Description ? ("<p>Pre-Mach Metric Children Description: " + stingray.charts[x].Children[y].Children[z].Description + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Children[z].Flags ? ("<p>Pre-Mach Metric Children Flags: " + stingray.charts[x].Children[y].Children[z].Flags + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Children[z].GUID ? ("<p>Pre-Mach Metric Children GUID: " + stingray.charts[x].Children[y].Children[z].GUID + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Children[z].Name ? ("<p>Pre-Mach Metric Children Name: " + stingray.charts[x].Children[y].Children[z].Name + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Children[z].ObjectType ? ("<p>Pre-Mach Metric Children ObjectType: " + stingray.charts[x].Children[y].Children[z].ObjectType + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Children[z].ParentID ? ("<p>Pre-Mach Metric Children ParentID: " + stingray.charts[x].Children[y].Children[z].ParentID + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Children[z].ScaleType ? ("<p>Pre-Mach Metric Children ScaleType: " + stingray.charts[x].Children[y].Children[z].ScaleType + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Children[z].SourceID ? ("<p>Pre-Mach Metric Children SourceID: " + stingray.charts[x].Children[y].Children[z].SourceID + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Children[z].Value ? ("<p>Pre-Mach Metric Children Value: " + stingray.charts[x].Children[y].Children[z].Value + "</p>") : "");
                    }
                }
                if (stingray.charts[x].Children[y].Filters) {
                    for (var z = 0; z < stingray.charts[x].Children[y].Filters.length; z++) {
                        HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Filters[z].ObjectType ? ("<p>Pre-Mach Metric Children Filters ObjectType: " + stingray.charts[x].Children[y].Filters[z].ObjectType + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Filters[z].Operator ? ("<p>Pre-Mach Metric Children Filters Operator: " + stingray.charts[x].Children[y].Filters[z].Operator + "</p>") : "");
                        if (stingray.charts[x].Children[y].Filters[z].Filters) {
                            for (var j = 0; j < stingray.charts[x].Children[y].Filters[z].Filters.length; j++) {
                                HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Filters[z].Filters[j].ID ? ("<p>Pre-Mach Metric Children Filters Filters ID: " + stingray.charts[x].Children[y].Filters[z].Filters[j].ID + "</p>") : "");
                                HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Filters[z].Filters[j].Action ? ("<p>Pre-Mach Metric Children Filters Filters Action: " + stingray.charts[x].Children[y].Filters[z].Filters[j].Action + "</p>") : "");
                                HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Filters[z].Filters[j].Count ? ("<p>Pre-Mach Metric Children Filters Filters Count: " + stingray.charts[x].Children[y].Filters[z].Filters[j].Count + "</p>") : "");
                                HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Filters[z].Filters[j].Description ? ("<p>Pre-Mach Metric Children Filters Filters Description: " + stingray.charts[x].Children[y].Filters[z].Filters[j].Description + "</p>") : "");
                                HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Filters[z].Filters[j].Flags ? ("<p>Pre-Mach Metric Children Filters Filters Flags: " + stingray.charts[x].Children[y].Filters[z].Filters[j].Flags + "</p>") : "");
                                HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Filters[z].Filters[j].Name ? ("<p>Pre-Mach Metric Children Filters Filters Name: " + stingray.charts[x].Children[y].Filters[z].Filters[j].Name + "</p>") : "");
                                HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Filters[z].Filters[j].ObjectType ? ("<p>Pre-Mach Metric Children Filters Filters ObjectType: " + stingray.charts[x].Children[y].Filters[z].Filters[j].ObjectType + "</p>") : "");
                                HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Filters[z].Filters[j].Operator ? ("<p>Pre-Mach Metric Children Filters Filters Operator: " + stingray.charts[x].Children[y].Filters[z].Filters[j].Operator + "</p>") : "");
                                HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Filters[z].Filters[j].ParentID ? ("<p>Pre-Mach Metric Children Filters Filters ParentID: " + stingray.charts[x].Children[y].Filters[z].Filters[j].ParentID + "</p>") : "");
                                HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Filters[z].Filters[j].ScaleType ? ("<p>Pre-Mach Metric Children Filters Filters ScaleType: " + stingray.charts[x].Children[y].Filters[z].Filters[j].ScaleType + "</p>") : "");
                                HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Filters[z].Filters[j].SourceID ? ("<p>Pre-Mach Metric Children Filters Filters SourceID: " + stingray.charts[x].Children[y].Filters[z].Filters[j].SourceID + "</p>") : "");
                                if (stingray.charts[x].Children[y].Filters[z].Filters[j].Values) {
                                    for (var k = 0; k < stingray.charts[x].Children[y].Filters[z].Filters[j].Values.length; k++) {
                                        HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Filters[z].Filters[j].Values[k].ID ? ("<p>Pre-Mach Metric Children Filters Filters Value ID: " + stingray.charts[x].Children[y].Filters[z].Filters[j].Values[k].ID + "</p>") : "");
                                        HTMLstring = HTMLstring + (stingray.charts[x].Children[y].Filters[z].Filters[j].Values[k].ObjectType ? ("<p>Pre-Mach Metric Children Filters Filters Value ObjectType: " + stingray.charts[x].Children[y].Filters[z].Filters[j].Values[k].ObjectType + "</p>") : "");
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if (stingray.charts[x].Ch) {
            for (var y = 0; y < stingray.charts[x].Ch.length; y++) {
                HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].C ? ("<p>Post-Mach Metric C: " + stingray.charts[x].Ch[y].C + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].D.AnswerDataType ? ("<p>Post-Mach Metric D.AnswerDataType: " + stingray.charts[x].Ch[y].D.AnswerDataType + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].D.showGoal ? ("<p>Post-Mach Metric D.showGoal: " + stingray.charts[x].Ch[y].D.showGoal + "</p>") : "");
                HTMLstring = HTMLstring + ("<p>Post-Mach Metric Date Filter: " + getDateFilterInterpretation(stingray.charts[x].Ch[y].D.dateType, stingray.charts[x].Ch[y].D.datePreset, stingray.charts[x].Ch[y].D.dateRelativeNum, stingray.charts[x].Ch[y].D.dateRelativeNum) + "</p>");
                HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].D.dashboardGUID ? ("<p>Post-Mach Metric D.dashboardGUID: " + stingray.charts[x].Ch[y].D.dashboardGUID + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].D.goal ? ("<p>Post-Mach Metric D.goal: " + stingray.charts[x].Ch[y].D.goal + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].D.filter ? ("<p>Post-Mach Metric D.filter: " + stingray.charts[x].Ch[y].D.filter + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].D.originalGuid ? ("<p>Post-Mach Metric D.originalGuid: " + stingray.charts[x].Ch[y].D.originalGuid + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].D.dateIncludeCurrent ? ("<p>Post-Mach Metric D.dateIncludeCurrent: " + stingray.charts[x].Ch[y].D.dateIncludeCurrent + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].D.savedFilterId ? ("<p>Post-Mach Metric D.savedFilterId: " + stingray.charts[x].Ch[y].D.savedFilterId + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].ED ? ("<p>Post-Mach Metric ED: " + stingray.charts[x].Ch[y].ED + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].Fl ? ("<p>Post-Mach Metric Fl: " + stingray.charts[x].Ch[y].Fl + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].PID ? ("<p>Post-Mach Metric PID: " + stingray.charts[x].Ch[y].PID + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].V ? ("<p>Post-Mach Metric V: " + stingray.charts[x].Ch[y].V + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].VT ? ("<p>Post-Mach Metric VT: " + stingray.charts[x].Ch[y].VT + "</p>") : "");
                if (stingray.charts[x].Ch[y].BDs) {
                    for (var z = 0; z < stingray.charts[x].Ch[y].BDs.length; z++) {
                        HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].BDs[z].PID ? ("<p>Post-Mach Metric BDs PID: " + stingray.charts[x].Ch[y].BDs[z].PID + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].BDs[z].SID ? ("<p>Post-Mach Metric BDs SID: " + stingray.charts[x].Ch[y].BDs[z].SID + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].BDs[z].OT ? ("<p>Post-Mach Metric BDs OT: " + stingray.charts[x].Ch[y].BDs[z].OT + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].BDs[z].N ? ("<p>Post-Mach Metric BDs N: " + stingray.charts[x].Ch[y].BDs[z].N + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].BDs[z].D ? ("<p>Post-Mach Metric BDs D: " + stingray.charts[x].Ch[y].BDs[z].D + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].BDs[z].A ? ("<p>Post-Mach Metric BDs A: " + stingray.charts[x].Ch[y].BDs[z].A + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].BDs[z].C ? ("<p>Post-Mach Metric BDs C: " + stingray.charts[x].Ch[y].BDs[z].C + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].BDs[z].V ? ("<p>Post-Mach Metric BDs V: " + stingray.charts[x].Ch[y].BDs[z].V + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].BDs[z].ST ? ("<p>Post-Mach Metric BDs ST: " + stingray.charts[x].Ch[y].BDs[z].ST + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].BDs[z].Fl ? ("<p>Post-Mach Metric BDs Fl: " + stingray.charts[x].Ch[y].BDs[z].Fl + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].BDs[z].Fl2 ? ("<p>Post-Mach Metric BDs Fl2: " + stingray.charts[x].Ch[y].BDs[z].Fl2 + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].BDs[z].Ts ? ("<p>Post-Mach Metric BDs Ts: " + stingray.charts[x].Ch[y].BDs[z].Ts + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].BDs[z].ED ? ("<p>Post-Mach Metric BDs ED: " + stingray.charts[x].Ch[y].BDs[z].ED + "</p>") : "");
                    }
                }
                if (stingray.charts[x].Ch[y].Ch) {
                    for (var z = 0; z < stingray.charts[x].Ch[y].Ch.length; z++) {
                        HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].Ch[z].AID ? ("<p>Post-Mach Metric Ch AID: " + stingray.charts[x].Ch[y].Ch[z].AID + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].Ch[z].PID ? ("<p>Post-Mach Metric Ch PID: " + stingray.charts[x].Ch[y].Ch[z].PID + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].Ch[z].SID ? ("<p>Post-Mach Metric Ch SID: " + stingray.charts[x].Ch[y].Ch[z].SID + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].Ch[z].OT ? ("<p>Post-Mach Metric Ch OT: " + stingray.charts[x].Ch[y].Ch[z].OT + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].Ch[z].N ? ("<p>Post-Mach Metric Ch N: " + stingray.charts[x].Ch[y].Ch[z].N + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].Ch[z].D ? ("<p>Post-Mach Metric Ch D: " + stingray.charts[x].Ch[y].Ch[z].D + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].Ch[z].A ? ("<p>Post-Mach Metric Ch A: " + stingray.charts[x].Ch[y].Ch[z].A + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].Ch[z].C ? ("<p>Post-Mach Metric Ch C: " + stingray.charts[x].Ch[y].Ch[z].C + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].Ch[z].V ? ("<p>Post-Mach Metric Ch V: " + stingray.charts[x].Ch[y].Ch[z].V + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].Ch[z].VID ? ("<p>Post-Mach Metric Ch VID: " + stingray.charts[x].Ch[y].Ch[z].VID + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].Ch[z].ST ? ("<p>Post-Mach Metric Ch ST: " + stingray.charts[x].Ch[y].Ch[z].ST + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].Ch[z].Fl ? ("<p>Post-Mach Metric Ch Fl: " + stingray.charts[x].Ch[y].Ch[z].Fl + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].Ch[z].Fl2 ? ("<p>Post-Mach Metric Ch Fl2: " + stingray.charts[x].Ch[y].Ch[z].Fl2 + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].Ch[z].Ts ? ("<p>Post-Mach Metric Ch Ts: " + stingray.charts[x].Ch[y].Ch[z].Ts + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].Ch[z].ED ? ("<p>Post-Mach Metric Ch ED: " + stingray.charts[x].Ch[y].Ch[z].ED + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].Ch[z].VT ? ("<p>Post-Mach Metric Ch VT: " + stingray.charts[x].Ch[y].Ch[z].VT + "</p>") : "");
                        if (stingray.charts[x].Ch[y].Ch[z].Vs) {
                            for (var j = 0; j < stingray.charts[x].Ch[y].Ch[z].Vs.length; j++) {
                                HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].Ch[z].Vs[j].ID ? ("<p>Post-Mach Metric Ch Vs ID: " + stingray.charts[x].Ch[y].Ch[z].Vs[j].ID + "</p>") : "");
                                HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].Ch[z].Vs[j].GUID ? ("<p>Post-Mach Metric Ch Vs GUID: " + stingray.charts[x].Ch[y].Ch[z].Vs[j].GUID + "</p>") : "");
                                HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].Ch[z].Vs[j].SID ? ("<p>Post-Mach Metric Ch Vs SID: " + stingray.charts[x].Ch[y].Ch[z].Vs[j].SID + "</p>") : "");
                                HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].Ch[z].Vs[j].OT ? ("<p>Post-Mach Metric Ch Vs OT: " + stingray.charts[x].Ch[y].Ch[z].Vs[j].OT + "</p>") : "");
                                HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].Ch[z].Vs[j].N ? ("<p>Post-Mach Metric Ch Vs N: " + stingray.charts[x].Ch[y].Ch[z].Vs[j].N + "</p>") : "");
                                HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].Ch[z].Vs[j].D ? ("<p>Post-Mach Metric Ch Vs D: " + stingray.charts[x].Ch[y].Ch[z].Vs[j].D + "</p>") : "");
                                HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].Ch[z].Vs[j].V ? ("<p>Post-Mach Metric Ch Vs V: " + stingray.charts[x].Ch[y].Ch[z].Vs[j].V + "</p>") : "");
                                HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].Ch[z].Vs[j].ED ? ("<p>Post-Mach Metric Ch Vs ED: " + stingray.charts[x].Ch[y].Ch[z].Vs[j].ED + "</p>") : "");
                            }
                        }
                    }
                }
                for (var z = 0; z < stingray.charts[x].Ch[y].Ts.length; z++) {
                    HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].Ts[z].N ? ("<p>Post-Mach Metric Ts N: " + stingray.charts[x].Ch[y].Ts[z].N + "</p>") : "");
                    HTMLstring = HTMLstring + (stingray.charts[x].Ch[y].Ts[z].S ? ("<p>Post-Mach Metric Ts S: " + stingray.charts[x].Ch[y].Ts[z].S + "</p>") : "");
                }
            }
        }
        //Getting all of the data for the Breakdown Level Info
        if (stingray.charts.Breakdowns) {
            for (var y = 0; y < stingray.charts.Breakdowns.length; y++) {
                HTMLstring = HTMLstring + (stingray.charts[x].Breakdowns[y].ID ? ("<p>Pre-Mach Breakdowns ID:" + stingray.charts[x].Breakdowns[y].ID + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Breakdowns[y].Action ? ("<p>Pre-Mach Breakdowns Action:" + stingray.charts[x].Breakdowns[y].Action + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Breakdowns[y].AttributeID ? ("<p>Pre-Mach Breakdowns AttributeID:" + stingray.charts[x].Breakdowns[y].AttributeID + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Breakdowns[y].Count ? ("<p>Pre-Mach Breakdowns Count:" + stingray.charts[x].Breakdowns[y].Count + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Breakdowns[y].Description ? ("<p>Pre-Mach Breakdowns Description:" + stingray.charts[x].Breakdowns[y].Description + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Breakdowns[y].Flags ? ("<p>Pre-Mach Breakdowns Flags:" + stingray.charts[x].Breakdowns[y].Flags + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Breakdowns[y].Flags2 ? ("<p>Pre-Mach Breakdowns Flags2:" + stingray.charts[x].Breakdowns[y].Flags2 + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Breakdowns[y].GUID ? ("<p>Pre-Mach Breakdowns GUID:" + stingray.charts[x].Breakdowns[y].GUID + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Breakdowns[y].Name ? ("<p>Pre-Mach Breakdowns Name:" + stingray.charts[x].Breakdowns[y].Name + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Breakdowns[y].Operator ? ("<p>Pre-Mach Breakdowns Operator:" + stingray.charts[x].Breakdowns[y].Operator + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Breakdowns[y].ObjectType ? ("<p>Pre-Mach Breakdowns ObjectType:" + stingray.charts[x].Breakdowns[y].ObjectType + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Breakdowns[y].ParentID ? ("<p>Pre-Mach Breakdowns ParentID:" + stingray.charts[x].Breakdowns[y].ParentID + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Breakdowns[y].ScaleType ? ("<p>Pre-Mach Breakdowns ScaleType:" + stingray.charts[x].Breakdowns[y].ScaleType + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Breakdowns[y].SourceID ? ("<p>Pre-Mach Breakdowns SourceID:" + stingray.charts[x].Breakdowns[y].SourceID + "</p>") : "");
                HTMLstring = HTMLstring + (stingray.charts[x].Breakdowns[y].Value ? ("<p>Pre-Mach Breakdowns Value:" + stingray.charts[x].Breakdowns[y].Value + "</p>") : "");
            }
            HTMLstring = HTMLstring + (stingray.charts[x].Breakdowns[y].ValueID ? ("<p>Pre-Mach Breakdowns ValueID:" + stingray.charts[x].Breakdowns[y].ValueID + "</p>") : "");
            if (stingray.charts.Breakdowns[y].Values) {
                for (var z = 0; z < stingray.charts.Breakdowns[y].Values.length; z++) {
                    HTMLstring = HTMLstring + (stingray.charts[x].Breakdowns[y].Values[z].ID ? ("<p>Pre-Mach Breakdowns Values ID:" + stingray.charts[x].Breakdowns[y].Values[z].ID + "</p>") : "");
                    HTMLstring = HTMLstring + (stingray.charts[x].Breakdowns[y].Values[z].Name ? ("<p>Pre-Mach Breakdowns Values Name:" + stingray.charts[x].Breakdowns[y].Values[z].Name + "</p>") : "");
                    HTMLstring = HTMLstring + (stingray.charts[x].Breakdowns[y].Values[z].SourceID ? ("<p>Pre-Mach Breakdowns Values SourceID:" + stingray.charts[x].Breakdowns[y].Values[z].SourceID + "</p>") : "");
                    HTMLstring = HTMLstring + (stingray.charts[x].Breakdowns[y].Values[z].ObjectType ? ("<p>Pre-Mach Breakdowns Values ObjectType:" + stingray.charts[x].Breakdowns[y].Values[z].ObjectType + "</p>") : "");
                    HTMLstring = HTMLstring + (stingray.charts[x].Breakdowns[y].Values[z].Action ? ("<p>Pre-Mach Breakdowns Values Action:" + stingray.charts[x].Breakdowns[y].Values[z].Action + "</p>") : "");
                    HTMLstring = HTMLstring + (stingray.charts[x].Breakdowns[y].Values[z].Flags ? ("<p>Pre-Mach Breakdowns Values Flags:" + stingray.charts[x].Breakdowns[y].Values[z].Flags + "</p>") : "");
                    HTMLstring = HTMLstring + (stingray.charts[x].Breakdowns[y].Values[z].ParentID ? ("<p>Pre-Mach Breakdowns Values ParentID:" + stingray.charts[x].Breakdowns[y].Values[z].ParentID + "</p>") : "");
                    HTMLstring = HTMLstring + (stingray.charts[x].Breakdowns[y].Values[z].ScaleType ? ("<p>Pre-Mach Breakdowns Values ScaleType:" + stingray.charts[x].Breakdowns[y].Values[z].ScaleType + "</p>") : "");
                    HTMLstring = HTMLstring + (stingray.charts[x].Breakdowns[y].Values[z].GUID ? ("<p>Pre-Mach Breakdowns Values GUID:" + stingray.charts[x].Breakdowns[y].Values[z].GUID + "</p>") : "");
                    HTMLstring = HTMLstring + (stingray.charts[x].Breakdowns[y].Values[z].Value ? ("<p>Pre-Mach Breakdowns Values Value:" + stingray.charts[x].Breakdowns[y].Values[z].Value + "</p>") : "");
                }
            }
            if (stingray.charts.BDs) {
                for (var y = 0; y < stingray.charts.BDs.length; y++) {
                    HTMLstring = HTMLstring + (stingray.charts[x].BDs[y].AID ? ("<p>Post-Mach Breakdowns AID:" + stingray.charts[x].BDs[y].AID + "</p>") : "");
                    HTMLstring = HTMLstring + (stingray.charts[x].BDs[y].PID ? ("<p>Post-Mach Breakdowns PID:" + stingray.charts[x].BDs[y].PID + "</p>") : "");
                    HTMLstring = HTMLstring + (stingray.charts[x].BDs[y].SID ? ("<p>Post-Mach Breakdowns SID:" + stingray.charts[x].BDs[y].SID + "</p>") : "");
                    HTMLstring = HTMLstring + (stingray.charts[x].BDs[y].OT ? ("<p>Post-Mach Breakdowns OT:" + stingray.charts[x].BDs[y].OT + "</p>") : "");
                    HTMLstring = HTMLstring + (stingray.charts[x].BDs[y].N ? ("<p>Post-Mach Breakdowns N:" + stingray.charts[x].BDs[y].N + "</p>") : "");
                    HTMLstring = HTMLstring + (stingray.charts[x].BDs[y].D ? ("<p>Post-Mach Breakdowns D:" + stingray.charts[x].BDs[y].D + "</p>") : "");
                    HTMLstring = HTMLstring + (stingray.charts[x].BDs[y].A ? ("<p>Post-Mach Breakdowns A:" + stingray.charts[x].BDs[y].A + "</p>") : "");
                    HTMLstring = HTMLstring + (stingray.charts[x].BDs[y].C ? ("<p>Post-Mach Breakdowns C:" + stingray.charts[x].BDs[y].C + "</p>") : "");
                    HTMLstring = HTMLstring + (stingray.charts[x].BDs[y].VID ? ("<p>Post-Mach Breakdowns VID:" + stingray.charts[x].BDs[y].VID + "</p>") : "");
                    HTMLstring = HTMLstring + (stingray.charts[x].BDs[y].ST ? ("<p>Post-Mach Breakdowns ST:" + stingray.charts[x].BDs[y].ST + "</p>") : "");
                    HTMLstring = HTMLstring + (stingray.charts[x].BDs[y].Fl ? ("<p>Post-Mach Breakdowns Fl:" + stingray.charts[x].BDs[y].Fl + "</p>") : "");
                    HTMLstring = HTMLstring + (stingray.charts[x].BDs[y].Fl2 ? ("<p>Post-Mach Breakdowns Fl2:" + stingray.charts[x].BDs[y].Fl2 + "</p>") : "");
                    HTMLstring = HTMLstring + (stingray.charts[x].BDs[y].ED ? ("<p>Post-Mach Breakdowns ED:" + stingray.charts[x].BDs[y].ED + "</p>") : "");
                    HTMLstring = HTMLstring + (stingray.charts[x].BDs[y].V ? ("<p>Post-Mach Breakdowns V:" + stingray.charts[x].BDs[y].V + "</p>") : "");
                }
                HTMLstring = HTMLstring + (stingray.charts[x].BDs[y].O ? ("<p>Post-Mach Breakdowns O:" + stingray.charts[x].BDs[y].O + "</p>") : "");
                if (stingray.charts.BDs[x].Ts) {
                    for (var z = 0; z < stingray.charts.BDs[x].Ts.length; z++) {
                        HTMLstring = HTMLstring + (stingray.charts[x].BDs[y].Ts[z].N ? ("<p>Post-Mach Breakdowns Ts[z].N:" + stingray.charts[x].BDs[y].Ts[z].N + "</p>") : "");
                        HTMLstring = HTMLstring + (stingray.charts[x].BDs[y].Ts[z].S ? ("<p>Post-Mach Breakdowns Ts[z].S:" + stingray.charts[x].BDs[y].Ts[z].S + "</p>") : "");
                    }
                }
            }
        }
        */
        HTMLstring = HTMLstring + "</stingraysection>";
    }
    return HTMLstring;
}

function getWidgetsReport() {
    var HTMLstring = "";
    HTMLstring = HTMLstring + "<stingraysection>" +
        "<h2>Widgets</h2>";
    for (var x = 0; x < stingray.widgets.length; x++) {
        HTMLstring = HTMLstring +
            "<stingrayChartPanel>" +
            "<p>ID: " + stingray.widgets[x].ID + "</p>" +
            "<p>Localization Key: " + stingray.widgets[x].LocalizationKey + "</p>" +
            "<p>Info: " + stingray.widgets[x].Value + "</p>" +
            "</stingrayChartPanel>";
    }
    HTMLstring = HTMLstring + "</stingraysection>"
    return HTMLstring;
}

function endHTMLReport() {
    return `</stingRayReport> </body> </html>`;
}




/*************************************************
 * Hierarchy Tree for Viewing Data not in report *
 *************************************************/
function createDataTree() {
    var generatedReportString;
    generatedReportString = getDataTreeReport();
    setTimeout(function() {
        newwindow = window.open();
        newdocument = newwindow.document;
        newdocument.write(generatedReportString);
        newdocument.close();
    }, 50);
}

function getDataTreeReport() { return `
<!DOCTYPE html>
<html>

<head>
    <script src="https://d3js.org/d3.v3.min.js" async></script>
    <meta content="text/html; charset=UTF-8" http-equiv="content-type">
    <style>
    #delimiter {
        width: 15px;
        text-align: center;
    }

    .node {
        cursor: pointer;
    }

    .overlay {
        background-color: #EEE;
    }

    .node circle {
        fill: #fff;
        stroke: steelblue;
        stroke-width: 1.5px;
    }

    .node text {
        font: 10px sans-serif;
    }

    .link {
        fill: none;
        stroke: #ccc;
        stroke-width: 1.5px;
    }

    #tree-container {
        float: left;
    }

    #accordian-list {
        float: left;
        width: auto;
        background-color: lightblue;
    }

    ul {
        list-style: none;
        margin: 0;
        padding: 0;
    }

    label {
        display: block;
        cursor: pointer;
        padding: 0px;
        border: 1px solid #fff;
        border-bottom: none;
    }

    label:hover {
        background: #26C201;
    }

    label.last {
        border-bottom: 1px solid #fff;
    }

    ul ul li {
        padding: 0px;
        padding-left: 15px;
        background: #59ABE3;
    }

    input[type="checkbox"] {
        position: absolute;
        left: -9999px;
    }

    input[type="checkbox"]~ul {
        height: 0;
        transform: scaleY(0);
    }

    input[type="checkbox"]:checked~ul {
        height: 100%;
        transform-origin: top;
        transition: transform .2s ease-out;
        transform: scaleY(1);
    }

    input[type="checkbox"]:checked+label {
        background: #26C281;
        border-bottom: 1px solid #fff;
    }

    td,
    th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
    </style>
    <title>Dashboard Data Tree</title>
</head>

<body>
    <select id="hierarchySelect" onchange="setUpHierarchy()"></select>
    <!--        <select id="hierarchySelect" onchange="loadHierarchy()"></select>  -->
    <div style="background-color: #737f23; width: 20px; height: 20px;">
    </div>
    <div style="color: #737f23;">
        Interaction Point
    </div>
    <button onclick="expand(root.children.forEach(expand)); update(root);">Expand All</button>
    <button onclick="collapse(root.children.forEach(collapse)); update(root);">Collapse All</button>
    <br/>
    <div>
        <div id="tree-container"></div>
    </div>
    <div>
        <ul id="accordian-list">
        </ul>
    </div>
</body>
<script>
var data = window.opener.stingray;
var stingray = data;
//////////Set Up Vars\\\\\\\\\\
var margin = { top: 20, right: 120, bottom: 20, left: 120 },
    width = 1024 - margin.right - margin.left, //***Adjust this one for the width of the window
    height = 768 - margin.top - margin.bottom; //***Adjust this one for the height of the window

var i = 0,
    duration = 750, //***Adjust this one for speed of transition
    root;

// size of the diagram
//var viewerHeight = document.body.clientHeight;
//var viewerWidth = document.body.clientWidth;
var viewerWidth = 1024;
var viewerHeight = 768;
//////////End Set Up Vars\\\\\\\\\\

/*
possible vars to delete if things get too bulky, or too slow:
stingray
nodeCount
map
opener
*/

//////////Parsers\\\\\\\\\\
var map = {},
    node, roots = []; //Initialize Vars
var data = [];
nodeCount = -1;

function rawToJSON(object, name, parentID) {
    var myID = nodeCount;
    //we create a node, and stick it in the data
    var temp = {};
    temp.id = myID;
    temp.name = name;
    temp.parentId = parentID;

    data.push(temp);
    //}
    //Recurse through the object, pulling out all the variables
    if (typeof(object) === "object") { //We need to recurse further down!
        for (var i in object) {
            nodeCount++;
            rawToJSON(object[i], i, myID)
        }
    } else {
        var temp = {};
        temp.id = (myID + "r");
        temp.name = object;
        temp.parentId = myID;
        data.push(temp);
    }
}

function JSONToTree(list) {
    for (i = 0; i < list.length; i += 1) {
        map[list[i].id] = i; // initialize the map
    }
    for (i = 0; i < list.length; i += 1) {
        node = list[i];
        if (node.parentId == "-1") {
            if (node.name != "stingray") roots.push(node);
        } else {
            var parentIDMapValue = map[node.parentId]; //Get the value of the parent ID of the node from the map
            if (!parentIDMapValue && parentIDMapValue != -1) { //This means that the parent doesn't exist in this list or map
                //Create the node
                var newNode = {
                    "id": node.parentId,
                    "name": node.parentId,
                    "parentId": "-1"
                }
                //create it in the map
                map[node.parentId] = list.length;
                parentIDMapValue = list.length;
                //create it in the list
                list.push(newNode);
            }
            if (!list[parentIDMapValue].children) {
                list[parentIDMapValue].children = [];
            }
            list[parentIDMapValue].children.push(node); //Push the node into it's children
        }
    }
    return roots;
}
//////////End Parsers\\\\\\\\\\



//////////List Setup\\\\\\\\\\
function recursivelyCreateList(tree, parent, listIDTrail) {
    //create the node in the list in the DOM
    var listItemElement = document.createElement("li");
    parent.appendChild(listItemElement);

    var inputElement = document.createElement("input");
    inputElement.setAttribute("type", "checkbox");
    inputElement.setAttribute("id", listIDTrail);
    listItemElement.appendChild(inputElement);

    var labelElement = document.createElement("label");
    labelElement.setAttribute("for", listIDTrail);
    listItemElement.appendChild(labelElement);

    var textnode = document.createTextNode(tree.name);
    labelElement.appendChild(textnode);

    var unorderedListElement = document.createElement("ul");
    listItemElement.appendChild(unorderedListElement);
    //pass the children as trees & the id trail
    if (tree.children) {
        for (var x = 0; x < tree.children.length; x++) {
            recursivelyCreateList(tree.children[x], unorderedListElement, listIDTrail + tree.children[x].id);
        }
    } else if (tree._children) {
        for (var x = 0; x < tree._children.length; x++) {
            recursivelyCreateList(tree._children[x], unorderedListElement, listIDTrail + tree._children[x].id);
        }
    }
};

function createList(listInfo) {
    //Remove all nodes from the tree
    var list = document.getElementById("accordian-list");
    while (list.firstChild) {
        list.removeChild(list.firstChild);
    }
    //put the data into the table
    //Traverse Tree through depth first search
    listInfo.forEach(function(element) {
        recursivelyCreateList(element, list, element.id);
    });
}
//////////End List Setup\\\\\\\\\\


//////////Tree Setup\\\\\\\\\\
// Define the zoom function for the zoomable tree
function zoom() {
    svgGroup.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
}

function collapse(d) {
    if (d) {
        if (d.children) {
            d._children = d.children;
            d._children.forEach(collapse);
            d.children = null;
        }
    }
}

function expand(d) {
    if (d) {
        if (d._children) {
            d.children = d._children;
            d.children.forEach(expand);
            d._children = null;
        }
    }
}

function setUpHierarchy() {
    var x = document.getElementById("hierarchySelect").value;
    var dataIndex = -1;
    for (i = 0; i < data.length; i += 1) {
        if (data[i].name === x) {
            dataIndex = i;
        }
    }

    root = data[dataIndex];
    root.x0 = 0;
    root.y0 = 0;
    if (root.children) root.children.forEach(collapse);
    update(root);

    d3.select(self.frameElement).style("height", "800px");
}

function update(source) { //Source refers to a single tree Hierarchy
    // Compute the new tree layout.
    var nodes = tree.nodes(root).reverse(),
        links = tree.links(nodes);

    // Normalize for fixed-depth.
    nodes.forEach(function(d) { d.y = d.depth * 200; }); //***Adjust this one for horizontal spacing

    // Update the nodes…
    var node = svgGroup.selectAll("g.node")
        .data(nodes, function(d) { return d.id || (d.id = ++i); });

    // Enter any new nodes at the parent's previous position.
    var nodeEnter = node.enter().append("g")
        .attr("class", "node")
        .attr("transform", function(d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
        .on("click", click);

    nodeEnter.append("circle")
        .attr("r", 1e-6)
        .style("fill", function(d) { return d._children ? "lightsteelblue" : "#fff"; });

    nodeEnter.append("text")
        //.attr("x", function(d) { return d.children || d._children ? -10 : 10; })
        .attr("x", function(d) { return -10; })
        .attr("dy", ".35em")
        //.attr("text-anchor", function(d) { return d.children || d._children ? "end" : "start"; })
        .attr("text-anchor", function(d) { return "end"; })
        .text(function(d) { return d.name; })
        .style("fill-opacity", 1e-6);

    // Transition nodes to their new position.
    var nodeUpdate = node.transition()
        .duration(duration)
        .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; });

    nodeUpdate.select("circle")
        .attr("r", 4.5)
        .style("fill", function(d) {
            if (!d.children && !d._children) { //If it's a leaf node
                return "#737f23";
            } else {
                return d._children ? "lightsteelblue" : "#fff";
            }
        })
        .style("stroke", function(d) {
            if (!d.children && !d._children) { //If it's a leaf node
                return "#737f23";
            } else {
                return "#3883b2";
            }
        });

    nodeUpdate.select("text")
        .style("fill-opacity", 1);

    // Transition exiting nodes to the parent's new position.
    var nodeExit = node.exit().transition()
        .duration(duration)
        .attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
        .remove();

    nodeExit.select("circle")
        .attr("r", 1e-6);

    nodeExit.select("text")
        .style("fill-opacity", 1e-6);

    // Update the links…
    var link = svgGroup.selectAll("path.link")
        .data(links, function(d) { return d.target.id; });

    // Enter any new links at the parent's previous position.
    link.enter().insert("path", "g")
        .attr("class", "link")
        .attr("d", function(d) {
            var o = { x: source.x0, y: source.y0 };
            return diagonal({ source: o, target: o });
        });

    // Transition links to their new position.
    link.transition()
        .duration(duration)
        .attr("d", diagonal);

    // Transition exiting nodes to the parent's new position.
    link.exit().transition()
        .duration(duration)
        .attr("d", function(d) {
            var o = { x: source.x, y: source.y };
            return diagonal({ source: o, target: o });
        })
        .remove();

    // Stash the old positions for transition.
    nodes.forEach(function(d) {
        d.x0 = d.x;
        d.y0 = d.y;
    });
}

// Toggle children on click.
function click(d) {
    if (d.parentId != 0) { //Makes it so you can't collapse the root node
        if (d.children) {
            d._children = d.children;
            d.children = null;
        } else {
            d.children = d._children;
            d._children = null;
        }
        update(d);
    }
    //Make it so the selected node is highlighted
}
//////////End Tree Setup\\\\\\\\\\



//////////Change Hierarchy\\\\\\\\\\
function loadHierarchy() {
    var x = document.getElementById("hierarchySelect").value;
}
//////////End Change Hierarchy\\\\\\\\\\

var zoomListener;
var baseSvg;
var tree;
var diagonal;
var svgGroup;
window.onload = function() {
    // define the zoomListener which calls the zoom function on the "zoom" event constrained within the scaleExtents
    zoomListener = d3.behavior.zoom().scaleExtent([0.05, 2]).on("zoom", zoom);

    baseSvg = d3.select("#tree-container").append("svg")
        .attr("width", viewerWidth)
        .attr("height", viewerHeight)
        .attr("class", "overlay")
        .call(zoomListener);

    tree = d3.layout.tree()
        .size([height, width]);

    diagonal = d3.svg.diagonal()
        .projection(function(d) { return [d.y, d.x]; });

    // Append a group which holds all nodes and which the zoom Listener can act upon.
    svgGroup = baseSvg.append("g");



    rawToJSON(stingray, "stingray", -1);
    //JSONToTree(data);
    setTimeout(function() {
        JSONToTree(data);
        for (i = 0; i < roots.length; i += 1) {
            var optionName = roots[i].name
            var z = document.createElement("option"); //Create the option
            z.setAttribute("value", optionName); //set the value
            var t = document.createTextNode(optionName);
            z.appendChild(t);
            document.getElementById("hierarchySelect").appendChild(z);
        }

        createList(roots);
        setUpHierarchy(roots);
    }, 25);

}
</script>

</html>
`; }





/***************************
 * Initiate Function Calls *
 ***************************/
stingrayGetReportData();

if (!document.getElementById('stingRayMain')) {
    createToggle();
    createChartBtn();
    createDashboardDataPanel();
    createChartDataPanel();
    createFunctionDataPanel();
}

updateChartData('init');

$('stingRayDashboardData').attr('style', 'background-color:' + stingray.colors.DataPanel);
$('stingRayChartData').attr('style', 'background-color:' + stingray.colors.DataPanel);
$('stingraychartbtn').attr('style', 'color:' + stingray.colors.CompanyColor);

$('stingraychartbtn').click(function() {
    clearBtnSelect();
    $(this).addClass('selectedSRC');
    updateChartData($(this).attr('chartid'));
    setChartDataPos();
});