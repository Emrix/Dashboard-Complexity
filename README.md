# Stingray

Dashboard Complexity Checker

**Purpose:** The purpose of this project is to find out the complexity of Dashboards in the system.  By copying and pasting the code into a browser console while looking at the Dashboard, it will spit out several metrics for Dashboard Complexity information.

## Object Type Translations
|Num|Description|
|---:|---|
|1|Source (Case or Survey)|
|2|Question (Text, Choose one, Choose many, basically everything besides a table)|
|3|Answer Option|
|4|*unknown*|
|5|Filter|
|6|A type of Zone (Double column?)|
|7|Charts|
|8|*unknown*|
|9|A Type of Zone (Single column?)|
|10|Images & Text Widgets|
|11|Infobar|
|12|Dashboard Filters and Dates|
|13|*unknown*|
|14|Hierarchy|
|15|*unknown*|
|16|*unknown*|
|17|Table|

## JSON Key Translations
|Term|Description|
|---:|---|
|A|Action|
|AID|*unknown*|
|BD|*unknown*|
|BDs|Break Downs|
|C|Count (number of answers)|
|Ch|Children|
|D|Description|
|ED|*unknown*|
|Fl|Flags|
|Fl2|Flags2|
|GUID|GUID ID|
|ID|ID|
|LK|*unknown*|
|N|Name|
|OT|ObjectType|
|PID|ParentID|
|SID|SourceID|
|ST|ScaleType (basic type of question)|
|Ts|*unknown* (Mach Timing Results)|
|VID|ValueID --> Included with Hierarchies|
|V|Value --> Included with Questions|
|Vs|*unknown* (Answer Options)|

## Scale Type Translations
|Num|Description|
|---:|---|
|1|Long Text / Activity Notes|
|2|Choose Many|
|3|Rank|
|4|Choose 1 / Single Checkbox / Escalated / Status|
|5|Short Text / File Upload / Textbox Numeric / Date / Case ID|
|6|Hierarchy|
|7|*unknown*|
|8|Table|

## Action Translations (Inside getReportResults)
|Num|Description|
|---:|---|
|1|Metric - Normal Calculation|
|2|*unknown*|
|3|*unknown*|
|4|Breakdown on a Metric - Either a breakdown or a stack|
|5|*Filter of a metric*|
|6|*unknown*|
|7|*unknown*|
|8|*unknown*|
|8|Split - Splits metrics into more metrics|

## Filter Type Translations
|Num|Description|
|---:|---|
|1|Match ANY of these criteria|
|2|Match ALL of these criteria|





## Information to pull
### Sources
*  Pull names
	`stingRay.sourceData[x][0].N`

* Types of sources
	`stingRay.sourceData[x][0].D`
	`Note: '{"AnswerDataType":"null"}' specifies a survey`

* Number of Questions / tokens in a source
	`stingRay.sourceData[x][0].Ch.length`

* Number of Answer Options per question
	`stingRay.sourceData[x][0].Ch[y].C`

* Type of Question (TA, Custom Field, Weighted answer)
	`stingRay.sourceData[x][0].Ch[y].ST`

* If Case programs have Surveys as children
	```
var chartTypesCount = window.chartTypes;
for (var x in chartTypesCount) {
	chartTypesCount[x].quantityInDB = 0;
}
for (var x = 0; x < stingRay.myCharts.data.length; x++) {
	chartTypesCount[stingRay.myCharts.data[x].D.chartType].quantityInDB++;
}
//Quantity Data is located in chartTypesCount[chartType].quantityInDB
	```

### Dashboard
* Number of Data Sources
	`stingRay.sourceData.length`
* Number of Charts
	`stingRay.myCharts.data.length`
* Number of types of charts
	```
var results = window.chartTypes;
for (var x in results) {
	results[x].quantityInDB = 0;
}
for (var x = 0; x < stingRay.myCharts.data.length; x++) {
	var tempChartData = stingRay.myCharts.data[x];
	tempChartData = JSON.parse(tempChartData.D);
	results[tempChartData.chartType].quantityInDB = results[tempChartData.chartType].quantityInDB + 1;
}
//Quantity Data is located in results[chartType].quantityInDB
	```
* Match Any Filter
	`stingRay.getReportResults.Filters[1].Filters.length`
* Match All Filter
	`stingRay.getReportResults.Filters[0].Filters.length`
* Text & image widgets
	`stingRay.myWidgets.length`


### Charts
* Chart Type
	`stingRay.myCharts.data[x].D.chartType`
* Number of metrics per chart
	`stingRay.myCharts.data[x].Ch.length`
* Number of breakdowns (A single breakdown applies to all metrics)
	`stingRay.myCharts.data[x].BDs.length`
* Number of splits
	```
var chartInfo = stingRay.getReportResults.(find the chart);
var results = 0;
for (var x = 1; x < chartInfo.Ch[y].Ch.length; x++) {
	if (chartInfo.Ch[x].Ch[y].Action === 9) {
		results++;
	}
}
//Answer is contained in the results variable
	```
* Number of Stacks
	`stingRay.myCharts.data[x].Ch[y].BDs.length`
* Number of answer options in each breakdown
	`stingRay.data[x].BDs[y].C`
* Number of answer options in each metric and splits of metrics
	`stingRay.data[x].Ch[y].C`
* Data source of Breakdowns
	`stingRay.data[x].BDs[y].SID`
* Data source of Metrics
	```
var chartInfo = stingRay.getReportResults.(find the chart);
chartInfo.Ch[x].SourceID;
	```
* Data source of Splits
	```
var chartInfo = stingRay.getReportResults.(find the chart);
var results = 0;
if (chartInfo.Ch[x].Ch[y].Action === 9) {
	results = chartInfo.Ch[x].Ch[y].SourceID;
}
//Answer is contained in the results variable
	```
* Filters on Charts
	``
* Filters on metrics
	`stingRay.getReportResults.Children[0].Children[0].Children[0].Children[0].Children[x].Filters`
* Hierarchy on a token
	`stingRay.myCharts.data[x].BDs[y].OT === 14`
* Goal is Set
	``
* Extensibility Script
	`JSON.parse(stingRay.getReportResults.Children[0].Children[0].Children[1].Children[0].Description).extensibilityScript`
* Is Response List (Verbatim Tables?)
	`JSON.parse(stingRay.getReportResults.Children[0].Children[0].Children[1].Children[0].Description).isResponseList`
* Secondary Y-Axis
	`JSON.parse(stingRay.getReportResults.Children[0].Children[0].Children[1].Children[0].Description).useSecondaryYAxis`


### Tables
* Number of Rows in a table
	`JSON.parse(stingRay.getReportResults.Children[0].Children[0].Children[1].Children[0].Description).rows.length`
* Number of Columns in a table
	`JSON.parse(stingRay.getReportResults.Children[0].Children[0].Children[1].Children[0].Description).columns.length`
* Table is Transposed
	`JSON.parse(stingRay.getReportResults.Children[0].Children[0].Children[1].Children[0].Description).isTransposed`


### Timing
* Timing data from Mach for Breakdowns
	`stingRay.myCharts.data[x].BDs[y].TS`
* Timing data from Mach for Metrics
	`stingRay.myCharts.data[x].TS`


### Other
* List of Chart Types
	`this.chartTypes`

* Find a way to show that the query is still running (Some charts take a rediculous amount of time to load, so it would be nice to have a thing that says it's still running or not)